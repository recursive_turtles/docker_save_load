# Does Docker save/load work?

Yes it does.

# How?

Like this

```
./example.sh

+ docker build -t thisisatest .
Sending build context to Docker daemon  777.2kB
Step 1/2 : FROM busybox
 ---> 6d5fcfe5ff17
Step 2/2 : ADD "hi.txt" "/opt/hi.txt"
 ---> 80ab6e6d8e93
Successfully built 80ab6e6d8e93
Successfully tagged thisisatest:latest
+ docker save thisisatest
+ gzip
+ docker rmi thisisatest
Untagged: thisisatest:latest
Deleted: sha256:80ab6e6d8e9326a5f35a83c7e1eea523913a8bd150848ef78bcec6830d1908c9
Deleted: sha256:234646433e57715e1ad92276f4db71ba2e6b729b1e05ead645b28c0ee0a6ca92
+ docker load -i test.tar.gz
57ee76b151f0: Loading layer [==================================================>]   2.56kB/2.56kB
Loaded image: thisisatest:latest
+ docker image ls
+ grep thisisatest
thisisatest                                         latest              80ab6e6d8e93        Less than a second ago   1.22MB
```

# Ohhhh

Yeah
