#!/bin/bash -ex

docker build -t thisisatest .
docker save thisisatest | gzip > test.tar.gz
docker rmi thisisatest
docker load -i test.tar.gz
docker image ls | grep thisisatest